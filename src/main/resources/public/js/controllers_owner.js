ciaiApp.controller('ownerDashboardController', ['$q', '$filter', '$modal', '$routeParams', '$scope', '$http', '$location', 'authService', function($q, $filter, $modal, $routeParams, $scope, $http, $location, authService){
	console.log("ownerDasboardController");
	if(authService.authentication.isAuth && authService.authentication.role == 'Owner')
	{
		$scope.owner = authService.authentication.user;
		refreshHotels();
	}
	else
	{
		console.log("not an owner");
		$location.path("/");
	}

	$scope.today = new Date();
	$scope.showbookings = true;
	
	$scope.dateOptions = { formatYear: 'yy', startingDay: 1};
    $scope.format = 'yyyy-MM-dd';
    $scope.openDateEntry = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedDateExit = false;
        $scope.openedDateEntry = true;
    };
    
    $scope.openDateExit = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedDateExit = true;
        $scope.openedDateEntry = false;
    };
	
	$scope.goTo = function(id)
	{
		$location.path('/hotels/details/' + id);
	}

	$scope.edit = function(id)
	{
		$location.path('/hotels/edit/' + id);
	}

	$scope.remove = function(hotel)
	{
		if (confirm("Deseja remover o hotel " + hotel.name + " ?"))
			$http.post('/hotels/inactivate/' + hotel.id)
				.success(function(data,status){
					refreshHotels();
				})

	}

	function refreshHotels()
	{
		$http.get('/hotels/owner/' + $scope.owner.id)
			.success(function(data,status){
				$scope.hotels = data;
				angular.forEach($scope.hotels, function(hotel){
					$http.get('/hotels/waitingbookings/' + hotel.id)
						.success(function(data,status){
							hotel.waitingbookings = data.ids;
						})
						.error(function(data,status){
							console.log("error waitingbookings");
						})
				})
			})
	}

	$scope.showWaitingBookings = function(hotel)
	{
		var modalAcceptBooking = $modal.open({
            templateUrl: 'views/modals/acceptbooking.html',
            controller: 'modalAcceptBookingController',
            size: 'lg',
            resolve: {
                hotel: function () {
                    return hotel;
                }
            }
        });
	}
	
	function getHotelRooms(date)
    {
    	return $q(function(resolve,reject){
    		$http.get("/hotelrooms/hotel/"+ $scope.hotelid + "/free/" + date)
    			.success(function(data){
    				resolve(data);
    			})
    			.error(function(data){
    				console.log("error");
    				console.log(data);
    			})
    	})	
    }
    
    
    function checkIfIsTheEnd(days, rooms, startDate)
    {
    	rooms.date = angular.copy(startDate);
    	days.push(rooms);
    	startDate.setDate(startDate.getDate() + 1);
    	if(startDate < $scope.exitDate)
    		getHotelRooms($filter('date')(startDate, 'yyyy-MM-dd'))
    		.then(function(newRooms){
    			checkIfIsTheEnd(days, newRooms, startDate)
    		})
		else
		{
			$scope.days = days;
		}
    }
	
	$scope.seeOccupancy = function()
	{
		var firstDate = angular.copy($scope.entryDate);
    	
    	var days = [];
    	
    	getHotelRooms($filter('date')(firstDate, 'yyyy-MM-dd'))
    		.then(function(rooms){
    			checkIfIsTheEnd(days, rooms, firstDate)
    		})
	}
}])
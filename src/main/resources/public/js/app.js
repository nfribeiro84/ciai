'use strict';

var ciaiApp = angular.module("ciaiApp",
	[
		'ngRoute',
		'LocalStorageModule',
		'ui.bootstrap'
	]);


ciaiApp.config(function ($httpProvider)
{
    $httpProvider.interceptors.push('authInterceptorService');
});

ciaiApp.config(function (localStorageServiceProvider) {
    localStorageServiceProvider
      .setStorageType('sessionStorage');
});

ciaiApp.run(['authService', function (authService)
{
	authService.fillAuthData();
}]);


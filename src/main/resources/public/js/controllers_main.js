'use strict';

ciaiApp.controller('homeController', ['$scope', '$http', '$location', 'authService', function($scope, $http, $location, authService)
{
	$scope.authentication = authService.authentication;
	$scope.authors = ["António Matias", "Carlos Júlio", "Nuno Ribeiro"];
	$scope.logout = function()
	{
		authService.logout();
		$location.path('/');
	}
	
	switch(authService.authentication.role)
	{
		case 'Owner' : $scope.dashboardUrl = '#/owner/dashboard';
		break;
		case 'Moderator' : $scope.dashboardUrl = '#/moderator/dashboard';
		break;
		case 'Guest' : $scope.dashboardUrl = '#/guest/dashboard';
		break;
		case 'Admin' : $scope.dashboardUrl = '#/admin/dashboard';
		break;
		default: $scope.dashboardUrl = '#/';
		break;
	}
}])

.controller('headerController', ['$scope', '$location', 'authService', function($scope, $location, authService) {
	$scope.authentication = authService.authentication;
	console.log("headercontroller");
	console.log(authService.authentication.role);
	
	
	function setLink()
	{	
		switch(authService.authentication.role)
		{
		case 'Owner' : $scope.dashboardUrl = '#/owner/dashboard';
		break;
		case 'Moderator' : $scope.dashboardUrl = '#/moderator/dashboard';
		break;
		case 'Guest' : $scope.dashboardUrl = '#/guest/dashboard';
		break;
		case 'Admin' : $scope.dashboardUrl = '#/admin/dashboard';
		break;
		default: $scope.dashboardUrl = '#/';
		break;
		}
	}

	setLink();
	
	$scope.$watch('authentication.role', function(newValue, oldValue){
		if(newValue!= oldValue)
			setLink();
	})
	
	$scope.search = function()
	{
		if ($scope.searchText !== undefined)
			$location.path('search/' + $scope.searchText);
	}
	
	$scope.logout = function()
	{
		authService.logout();
		$location.path('/');
	}
}])

ciaiApp.controller('loginController', ['$scope', '$http', '$location', 'authService', function($scope, $http, $location, authService) {

	if (authService.authentication.isAuth)
		$location.path('/');
	else
	{
		$scope.message = "";
		$scope.login = function ()
		{
			if ($scope.user !== undefined && $scope.user.username !== undefined && $scope.user.password !== undefined)
				authService.login($scope.user).then(function (user)
				{
					switch(user.roles[0].name)
					{
						case 'Owner' : $location.path('owner/dashboard');
						break;
	                	case 'Moderator' : $location.path('moderator/dashboard');
	                	break;
	                	case 'Guest' : $location.path('guest/dashboard');
	                	break;
	                	default : $location.path('/');
	                	break;
	                }
				},
				function (err)
				{
					$scope.message = err.error_description;
				});
			else
				$scope.message = 'Password and username can\'t be blank';
		};
	}
	
	$scope.close = function()
	{
		$scope.message = undefined;
	}
}])

ciaiApp.controller('registrationController',['$scope', '$http', '$location', 'authService', function($scope, $http, $location, authService){
	//$scope.roles = ['guest', 'owner', 'moderator', 'admin'];

	$http.get('/roles').success(function(data, status)
	{
		$scope.roles = data;
	});
	
	$scope.newUser = {};

	$scope.save = function()
	{
		$http.post('/users/registration', $scope.newUser).success(function(data, status)
		{
			if (status == 200)
			{
				authService.login(data).then(function(response)
				{
					$location.path('/');
				})
			}
			else
				$scope.message = data.message;
		}).error(function(data, status)
		{
			$scope.message = data.message;
		})
	}
	
	$scope.close = function()
	{
		$scope.message = undefined;
	}
}])
'use strict';

ciaiApp
.controller('guestDashboardController', ['$modal', '$filter', '$q', '$scope', '$http', '$location', 'authService', function($modal, $filter, $q, $scope, $http, $location, authService){
	$scope.authentication = authService.authentication;
	if(!$scope.authentication.isAuth || $scope.authentication.role != 'Guest')
	{
		console.log("not a guest");
		$location.path('/')
	}
	else
	{
		$scope.today = new Date();
		$scope.showbookings = true;
		
		$scope.dateOptions = { formatYear: 'yy', startingDay: 1};
	    $scope.format = 'yyyy-MM-dd';
	    $scope.openDateEntry = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openedDateExit = false;
            $scope.openedDateEntry = true;
        };
        
        $scope.openDateExit = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openedDateExit = true;
            $scope.openedDateEntry = false;
        };
		
	    
	    $scope.search = function()
	    {
	    	$scope.showsearch = true;
	    	$scope.showbookings = false;
	    }
	    
	    $scope.showBookings = function()
	    {
	    	$scope.showSearch = false;
	    	$scope.showbookings = true;
	    }
		
	    function getHotelRooms(date)
	    {
	    	return $q(function(resolve,reject){
	    		$http.get("/hotelrooms/free/" + date)
	    			.success(function(data){
	    				resolve(data);
	    			})
	    			.error(function(data){
	    				console.log("error");
	    				console.log(data);
	    			})
	    	})	
	    }
	    
	    
	    function checkIfIsTheEnd(hotelrooms, rooms, startDate)
	    {
	    	if(hotelrooms.length == 0)
				hotelrooms = rooms;
			else
			{
				angular.forEach(hotelrooms, function(room){
					var newRoom = $filter('filter')(rooms, {id: room.id}, true)[0];
					if(newRoom == undefined)
						{
						var id = '!'+room.id;
						hotelrooms = $filter('filter')(hotelrooms, {id: id});
						}
				})
			}
	    	
	    	startDate.setDate(startDate.getDate() + 1);
	    	if(startDate < $scope.exitDate)
	    		getHotelRooms($filter('date')(startDate, 'yyyy-MM-dd'))
	    		.then(function(newRooms){
	    			checkIfIsTheEnd(hotelrooms, newRooms, startDate)
	    		})
    		else
			{
    			$scope.hotelrooms = hotelrooms;
    			var escolhidos = [];
    			var result = [];
    			angular.forEach(hotelrooms, function(hotelroom){
    				if(!escolhidos[hotelroom.hotelid])
					{
    					var hotel = angular.copy(hotelroom.hotel);
    					hotel.rooms = $filter('filter')(hotelrooms, {hotelid: hotelroom.hotelid});
    					escolhidos[hotelroom.hotelid] = true;
    					result.push(hotel);
					}
    			})
    			$scope.result = result;
			}
	    }
	    
	    $scope.pesquisar = function()
	    {
	    	var firstDate = angular.copy($scope.entryDate);

	    	console.log("--------->", firstDate);	    	console.log("--------->", $scope.entryDate);
	    	
	    	var continuar = true;
	    	var i = 0;
	    	var hotelrooms = [];
	    	
	    	getHotelRooms($filter('date')(firstDate, 'yyyy-MM-dd'))
	    		.then(function(rooms){
	    			checkIfIsTheEnd(hotelrooms, rooms, firstDate)
	    		})
	    }
	    
		myBookings().then(
			function(bookings){
				$scope.bookings = bookings;

				angular.forEach($scope.bookings, function(booking){
					$http.get('/hotelrooms/' + booking.hotelroomid)
						.success(function(hotelroom){
							booking.hotelroom = hotelroom;
							
							booking.canCancel = (booking.begin > $scope.today) && (booking.status == 'accepted' || booking.status == 'waiting');

							//get room type
							$http.get('/roomtypes/' + hotelroom.roomtypeid)
								.success(function(roomtype){
									booking.roomtype = roomtype;
								})

							$http.get('/hotels/' + hotelroom.hotelid)
								.success(function(hotel){
									booking.hotel = hotel;
								})
						})
				})
			}, 
			function(){
				//erro getting my bookings
				$location.path('/');
			})
	}
		
	function myBookings()
	{
		return $q(function(resolve, reject){
			$http.get('/bookings/user/' + $scope.authentication.user.id)
				.success(function(data, status){
					resolve(data);
				})
				.error(function(data,status){
					console.log(data);
					reject();
				})
		})
	}

	$scope.cancelBooking = function(booking)
	{
		var object = angular.copy(booking);
		object.status = 'canceled';
		$http.post('/bookings', object)
			.success(function(data,status){
				booking.status = 'canceled';
				booking.canCancel = false;
			})
	}
	
	$scope.reservar = function(room)
	{
		room.hotelname = room.hotel.name;
		room.name = room.roomType.name;
		var modalBookRoom = $modal.open({
            templateUrl: 'views/modals/bookroom.html',
            controller: 'modalBookRoomController',
            size: 'lg',
            resolve: {
                room: function () {
                    return room;
                },
                range: function(){
                	return {begin: $filter('date')($scope.entryDate, 'yyyy-MM-dd'), end: $filter('date')($scope.exitDate, 'yyyy-MM-dd')}
                }
            }
        });

        modalBookRoom.result.then(function () { $location.path('/guest/dashboard') });
	}

}])
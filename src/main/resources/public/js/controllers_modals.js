'use strict';

ciaiApp

.controller('modalBookRoomController', ['$filter', 'authService', '$scope', '$modalInstance', '$http', 'room', 'range', function ($filter, authService, $scope, $modalInstance, $http, room, range) {

	$scope.room = room;
       
	$scope.booking = {
			created: new Date(), 
			status: 'waiting', 
			price: room.price, 
			hotelroomid: room.id, 
			userid: authService.authentication.user.id
	};

	if(range == null)
	{
		$scope.noRange = true;
		$scope.dateOptions = {
				formatYear: 'yy',
				showWeeks: 'false',
		};
		
		$scope.format = 'yyyy-MM-dd';
		$scope.today = $filter('date')(new Date(), 'yyyy-MM-dd');
		$scope.beginDate = $filter('date')(new Date(), 'yyyy-MM-dd');
		$scope.endDate = $filter('date')(new Date(), 'yyyy-MM-dd');
	
	}
	else
		{
			$scope.booking.begin = $filter('date')(range.begin, 'yyyy-MM-dd');
			$scope.booking.end = $filter('date')(range.end, 'yyyy-MM-dd');
		}
    


    // função que envia para a api o texto a enviar no email de notificação
    // se o email for efectuado com sucesso, retorna a data com que o campo de notificacao 
    $scope.reserve = function () {
    	
    	if($scope.noRange)
    		{
	    		$scope.booking.begin = $filter('date')($scope.beginDate, 'yyyy-MM-dd');
	    		$scope.booking.end = $filter('date')($scope.endDate, 'yyyy-MM-dd');
    		}

        
        $http.post('/bookings', $scope.booking)
            .success(function (data, status) {
                console.log("RESERVADO");
                $modalInstance.close();
            })
            .error(function (data, status) {
                console.log("ERRO A RESERVAR");
            })
    }

    $scope.back = function () {
        $modalInstance.dismiss();
    }
}])

.controller('modalAcceptBookingController', ['$filter', 'authService', '$scope', '$modalInstance', '$http', 'hotel', function ($filter, authService, $scope, $modalInstance, $http, hotel) {
    
    $scope.hotel = hotel;
    $scope.bookings = [];

    angular.forEach($scope.hotel.waitingbookings, function(bookingid){
        $http.get('/bookings/' + bookingid)
            .success(function(booking){
                $scope.bookings.push(booking);
                
                $http.get('/users/' + booking.userid)
                    .success(function(user){
                        booking.user = user;
                    });

                $http.get('/hotelrooms/' + booking.hotelroomid)
                    .success(function(hotelroom){
                        booking.hotelroom = hotelroom;
                        $http.get('/roomtypes/' + hotelroom.roomtypeid)
                            .success(function(roomtype){
                                booking.roomtype = roomtype;
                            })
                    })
            })
    })

    $scope.accept = function(booking, newStatus)
    {
        booking.status = newStatus;
        $http.post('/bookings', booking)
            .success(function(data){
                $scope.bookings = $filter('filter')($scope.bookings, {id: '!'+booking.id});
            })
    }

    // função que envia para a api o texto a enviar no email de notificação
    // se o email for efectuado com sucesso, retorna a data com que o campo de notificacao 
    $scope.reserve = function () {
        $scope.booking.begin = $filter('date')($scope.beginDate, 'yyyy-MM-dd');
        $scope.booking.end = $filter('date')($scope.endDate, 'yyyy-MM-dd');

        
        $http.post('/bookings', $scope.booking)
            .success(function (data, status) {
                console.log("RESERVADO");
                $modalInstance.close();
            })
            .error(function (data, status) {
                console.log("ERRO A RESERVAR");
            })
    }

    $scope.back = function () {
        $modalInstance.dismiss();
    }
}])
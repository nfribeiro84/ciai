'use strict';

ciaiApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.    
    when("/",{
    	templateUrl: 'views/home.html',
    	controller: 'homeController'
    }).
    when("/login",{
        templateUrl: 'views/login.html',
        controller: 'loginController'
    }).
    when("/registration",{
        templateUrl: 'views/registration.html',
        controller: 'registrationController'
    }).
    
    // Hotels Routes
    when("/hotels",{
    	templateUrl: 'views/hotels/hotels.html',
    	controller: 'hotelsController'
    }).
    when("/hotels/details/:id", {
    	templateUrl: 'views/hotels/details.html',
    	controller: 'hotelsDetailsController'
    }).
    when("/hotels/new", {
        templateUrl: 'views/hotels/edit.html',
        controller: 'hotelsEditController'
    }).
    when("/hotels/edit/:id",{
    	templateUrl: 'views/hotels/edit.html',
    	controller: 'hotelsEditController'
    }).


    // Owner Routes
    when("/owner/dashboard",{
        templateUrl: 'views/owner/dashboard.html',
        controller: 'ownerDashboardController'
    }).

    // Moderator Routes
    when("/moderator/dashboard",{
        templateUrl: 'views/moderator/dashboard.html',
        controller: 'moderatorDashboardController'
    }).

    // Guest Routes
    when('/guest/dashboard',{
        templateUrl: 'views/guest/dashboard.html',
        controller: 'guestDashboardController'
    }).
    when('/search/:searchText',{
        templateUrl: 'views/search/results.html',
        controller: 'hotelsController'
    }).
    
    otherwise({
        redirectTo: '/',
    });
}]);
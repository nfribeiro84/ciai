'use strict';

ciaiApp.controller('moderatorDashboardController', ['$filter', '$scope', '$http', '$location', 'authService', function($filter, $scope, $http, $location, authService){
	var role = authService.authentication.role;
	$scope.authentication = authService.authentication;
	
	if (role != 'Moderator' && role != 'Admin')
	{
		console.log("no Admin and no Moderator");
		$location.path('/');
	}
	else
	{
		$scope.showwaiting = false;
		$http.get('/comments/status/waiting')
			.success(function(data,status){
				$scope.waitingList = data;
				
				angular.forEach($scope.waitingList, function(comment){
					$http.get('/comments/' + comment.id + '/user')
						.success(function(data,status){
							comment.user = data;
						})
				})
			})

		$http.get('/comments/status/rejected')
			.success(function(data,status){
				$scope.rejectedList = data;
			})
	}

	$scope.toggleShowWaiting = function()
	{
		$scope.showwaiting = !$scope.showwaiting;
	}

	$scope.saveComment = function(comment, newStatus)
	{
		comment.status = newStatus;
		$http.post('/comments', comment)
			.success(function(data, status){
				removeRejected(comment, newStatus == 'rejected');
			})
			.error(function(data,status){
				console.log("Erro a gravar o comentário: " + data);
				comment.status = 'waiting';
			})
	}

	function removeRejected(comment, rejected)
	{
		$scope.waitingList = $filter('filter')($scope.waitingList, {id: '!'+comment.id}, true);
		if(rejected)
			$scope.rejectedList.push(comment);
	}

}])
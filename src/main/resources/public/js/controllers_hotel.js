'use strict';

ciaiApp
.controller('hotelsController', ['$scope', '$routeParams', '$http', '$location', function($scope, $routeParams, $http, $location) {
	$http.get('/hotels').success(function(data, status)
	{
		$scope.hotels = data;
		$scope.searchText = $routeParams.searchText;
	});
		
	$scope.goTo = function(id)
	{
		$location.path('/hotels/details/' + id);
	}
}])

.controller('hotelsDetailsController', ['$modal', '$filter', '$q', '$scope', '$http', '$routeParams', '$location', 'authService', function($modal, $filter, $q, $scope, $http, $routeParams, $location, authService){
	$scope.extracomments = [];
	$scope.authentication = authService.authentication;
	$scope.userrole = $scope.authentication.role;
	$scope.isOwner = false;



	$http.get('/hotels/' + $routeParams.id)
		.success(function(data, status){
			$scope.hotel = data;
			$scope.isOwner = $scope.hotel.ownerid == authService.authentication.user.id;
			refreshComments();
		})
		.error(function(data,status){
			$location.path('/hotels');
		})

	$http.get('/hotelrooms/hotel/' + $routeParams.id)
		.success(function(rooms){
			$scope.rooms = rooms;
			setRoomTypesName();
		})

	function setRoomTypesName()
	{
		$http.get('/roomtypes')
			.success(function(roomtypes,status){
				angular.forEach($scope.rooms, function(room){
					room.name = $filter('filter')(roomtypes, {id: room.roomtypeid}, true)[0].name;
				})
			})
	}

	function refreshComments()
	{

		$http.get('/hotels/' + $routeParams.id + '/threads')
			.success(function(threads, status){
				$scope.threads = threads;
				angular.forEach($scope.threads, function(thread, index){
					$scope.extracomments[index] = false;
					$http.get('/threads/' + thread.id + '/comments')
						.success(function(comments, status){
							
							thread.firstComment = $filter('filter')(comments, {isFirst: true})[0];
							thread.comments = $filter('filter')(comments, {isFirst: false});
							thread.acceptedCommentsLength = $filter('filter')(thread.comments, {status: 'accepted'}).length;
							thread.waitingCommentsLength = $filter('filter')(thread.comments, {status: 'waiting'}).length;
							
							$http.get('/comments/' + thread.firstComment.id + '/user')
							.success(function(data,status){
								thread.firstComment.user = data;
							});
							
							angular.forEach(thread.comments, function(comment){
								$http.get('/comments/' + comment.id + '/user')
									.success(function(data,status){
										comment.user = data;
									})
							})
						})
				})
			})
	}

	$scope.reply = function(thread, index)
	{
		$scope.writeNewReply = true;
		$scope.newComment = 
		{
			threadid: thread.id, 
			userid: authService.authentication.user.id,
			isFirst: false,
			status: 'waiting',
			created: new Date()
		}

	}
	
	$scope.backNewReply = function()
	{
		$scope.newComment = null;
		$scope.writeNewReply = false;
	}

	$scope.comment = function(hotelid)
	{
		$scope.replies = [];
		$scope.newComment = 
		{
			userid: authService.authentication.user.id,
			isFirst: true,
			status: 'waiting',
			created: new Date(),
			hotelid: hotelid
		}
		$scope.writeNewComment = true;
	}
	
	$scope.backNewComment = function()
	{
		$scope.newComment = null;
		$scope.writeNewComment = false;
	}

	$scope.saveNewComment = function(newComment)
	{
		if(newComment)
		{
			//new comment
			$http.post('/comments/hotel/' + $scope.newComment.hotelid, $scope.newComment)
				.success(function(data,status){
					$scope.writeNewComment = false;
					refreshComments();
				})	
		}
		else
		{
			//reply
			$http.post('/comments', $scope.newComment)
				.success(function(data,status){
					refreshComments();
					$scope.writeNewReply = false;
				})	
		}
		$scope.hideResponse = false;
	}

	$scope.toggleReplies = function(index)
	{
		$scope.extracomments[index] = !$scope.extracomments[index];
	}
		
		
	$scope.edit = function()
	{
		$location.path('/hotels/edit/' + $scope.hotel.id);
	}
	
	$scope.remove = function()
	{	
		$http({method: 'DELETE', url: '/hotels/' + $scope.hotel.id })
			.success(function(data,status){
				if(status == 200)
					$location.path('/hotels');
				else
					console.log(data);
			})
			.error(function(data,status){
				console.log(data);
			});
	}

	$scope.changeCommentStatus = function(comment,newStatus)
	{
		var oldStatus = comment.status;
		comment.status = newStatus;
		$http.post('/comments', comment)
			.success(function(data,status){

			})
			.error(function(data,status){
				comment.status = oldStatus;
			})
	}

	$scope.bookroom = function(room)
	{
		room.hotelname = $scope.hotel.name;
		var modalBookRoom = $modal.open({
            templateUrl: 'views/modals/bookroom.html',
            controller: 'modalBookRoomController',
            size: 'lg',
            resolve: {
                room: function () {
                    return room;
                }
            }
        });

        modalBookRoom.result.then(function () { $location.path('/guest/dashboard') });
	}
}])

.controller('hotelsEditController', ['$filter', '$location', '$scope', '$http', '$routeParams', 'authService', function($filter, $location, $scope, $http, $routeParams, authService){
	$scope.authentication = authService.authentication;
	if(authService.authentication.isAuth && authService.authentication.Role == 'Owner')
	{
		getRoomTypes();

		if($routeParams.id == undefined)
		{
			$scope.action = "Novo Hotel";
			$scope.hotel = {ownerid: authService.authentication.user.id, status: 'waiting'};	
		}
		else
		{
			$scope.action = "Editar Hotel";
			getRooms();
			$http.get('/hotels/' + $routeParams.id)
				.success(function(data,status)
				{
					if(data.ownerid != authService.authentication.user.id)
						$location.path('/');
					else
					{
						$scope.hotel = data;
						$scope.hotel.status = 'waiting';
					}
				})
		}
	}
	else
	{
		console.log("não está autenticado");
		$location.path('/');
	}

	$scope.setNewRoom = function()
	{
		$scope.newRoom = { hotelid: $scope.hotel.id};
		$scope.setnewroom = true;
	}

	function getRoomTypes()
	{
		$http.get('/roomtypes')
			.success(function(data,status){
				$scope.roomtypes = data;
			})
	}

	function getRooms()
	{
		$http.get('/hotelrooms/hotel/' + $routeParams.id)
			.success(function(rooms){
				$scope.rooms = rooms;
				setRoomTypesName();
		})
	}
	
	
	function setRoomTypesName()
	{
		$http.get('/roomtypes')
			.success(function(roomtypes,status){
				angular.forEach($scope.rooms, function(room){
					room.name = $filter('filter')(roomtypes, {id: room.roomtypeid}, true)[0].name;
				})
			})
	}

	$scope.saveNewRoom = function()
	{
		$http.post('/hotelrooms', $scope.newRoom)
			.success(function(data,status){
				data.name = $filter('filter')($scope.roomtypes, {id: data.roomtypeid}, true)[0].name;
				$scope.rooms.push(data);
				$scope.newRoom = { };
				$scope.setnewroom = false;
			})
	}
	
	$scope.save = function()
	{
		$http.post('/hotels', $scope.hotel)
			.success(function(data,status){
				$scope.hotel = data;
				saveRooms();
			})
			.error(function(data,status){
				$scope.message = data.message;
				console.log("Não deu para gravar o hotel");
			})
	}

	function saveRooms()
	{
		angular.forEach($scope.rooms, function(room){
			$http.post('/hotelrooms', room);
		})
		$location.path('/hotels/details/' + $scope.hotel.id);
	}
	
}])

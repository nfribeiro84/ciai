ciaiApp
.factory('authService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {

    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        token: null,
        role:null,
        user: {}
    };

    var _login = function (loginData) {
    	
    	$http.defaults.headers.post.Authorization = 'Basic Y2xpZW50YXBwOjEyMzQ1Ng==';
        var deferred = $q.defer();
        $http.post('/oauth/token?password='+loginData.password+'&username='+loginData.username+'&grant_type=password&scope=read%20write&client_secret=123456&client_id=clientapp')
            .success(function(response){
            	var config = { headers: { 'Authorization': 'Bearer ' + response.access_token }};
            	
            	if (response.access_token != null && response.access_token != undefined)
                {
                    $http.get('/users/username/' + loginData.username, config)
                        .success(function (userData, status)
                        {
                        	if(status == 200)
                    		{
                        		saveLocalStorage(_authentication, localStorageService, response.access_token, userData);
                        		deferred.resolve(userData);
                    		}
                        	else
                        		deferred.reject("Ocorreu um erro ao efectuar o login");
                        });

                }//access_token is null
                else
                {
                    deferred.reject(response);
                }
            })
            .error(function(data,status){
                deferred.reject(data);
            })

        return deferred.promise;

    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.user = {};
        _authentication.token = null;
        _authentication.role = null;

    };

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.user = authData.user;
            _authentication.token = authData.token;
            _authentication.role = authData.user.roles[0].name;
        }
    }

    authServiceFactory.login = _login;
    authServiceFactory.logout = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;

    return authServiceFactory;
}])

.factory('authInterceptorService', ['$q', '$location', 'localStorageService', function ($q, $location, localStorageService) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
        	config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
    }

    var _responseError = function (rejection) {
        if (rejection.status === 401) {
        	localStorageService.remove('authorizationData')
        	$location.path('/login');
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);


function saveLocalStorage(_authentication, localStorageService, access_token, userData)
{
	console.log("savelocalstorage");
    localStorageService.set('authorizationData', { token: access_token, user: userData});
    _authentication.isAuth = true;
    _authentication.user = userData;
    _authentication.token = access_token;
    _authentication.role = userData.roles[0].name;
}
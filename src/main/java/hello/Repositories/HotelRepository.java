package hello.Repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import hello.Models.Hotel;

import java.util.*;


public interface HotelRepository extends CrudRepository<Hotel, Long> {
	
	Hotel findByName(String name);

	Iterable<Hotel> findByOwnerid(long ownerid);

	Iterable<Hotel> findByStatus(String status);

}


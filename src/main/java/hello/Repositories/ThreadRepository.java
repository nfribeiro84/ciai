package hello.Repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.*;

import hello.Models.Thread;


public interface ThreadRepository extends CrudRepository<Thread, Long> {
	
	Thread findByName(String name);
	Iterable<hello.Models.Thread> findByHotelid(long id);
}


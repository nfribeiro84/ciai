package hello.Repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.*;

import hello.Models.RoomType;

public interface RoomTypeRepository extends CrudRepository<RoomType, Long> {
	
	RoomType findByName(String name);
}


package hello.Repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import hello.Models.Comment;

import java.util.*;


public interface CommentRepository extends CrudRepository<Comment, Long> {
	
	Iterable<Comment> findByThreadid(long id);
	
	Iterable<Comment> findByStatus(String status);
}


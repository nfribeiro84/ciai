package hello.Repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.*;

import hello.Models.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	User findByName(String name);
	User findByUsername(String username);
}


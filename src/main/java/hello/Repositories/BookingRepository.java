package hello.Repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.*;

import hello.Models.Booking;

public interface BookingRepository extends CrudRepository<Booking, Long> {
	
	// Booking findByName(String name);
	Iterable<Booking> findByUserid(long id);
	
	Iterable<Booking> findByHotelroomid(long id);

	Iterable<Booking> findByStatus(String status);
	
	@Query("select b from Booking b inner join b.hotelroom r inner join r.hotel h where h.ownerid = ?")
	Iterable<Booking> getBookingsByOwner(Long ownerid);
	
	@Query("select b from Booking b where b.status = :status and "
				+ ":date = b.begin AND :date = b.end OR ("
					+ ":date >= b.begin AND :date < b.end )") //bookings de um dia apenas
	Iterable<Booking> getBookingsByDateAndStatus(@Param("date") Date date, @Param("status") String status);
	
	@Query("select b from Booking b inner join b.hotelroom r where b.status = :status and "
				+ "r.id = :hrid AND ("
				+ ":date = b.begin AND :date = b.end OR ("
					+ ":date >= b.begin AND :date < b.end ))") //bookings de um dia apenas
	Iterable<Booking> getBookingsByDateAndHotelroomidAndStatus(@Param("date") Date date, @Param("hrid") long hrid, @Param("status") String status);
}


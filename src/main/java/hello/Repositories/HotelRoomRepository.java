package hello.Repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.*;

import hello.Models.Booking;
import hello.Models.HotelRoom;


public interface HotelRoomRepository extends CrudRepository<HotelRoom, Long> {
	
	// HotelRoom findByName(String name);
	Iterable<HotelRoom> findByHotelid(long hotelid);
	
	Iterable<HotelRoom> findByHotelidAndRoomtypeid(long hotelid, long roomtypeid);

//	@Query("select r, (r.quantity - ( "
//				+ "select count(*) as total from Booking b where b.status = 'waiting' and "
//				+ "b.hotelroomid = r.id and "
//				+ ":date >= b.begin AND :date < b.end"	//booking começa antes ou no dia do user input e acaba depois do dia. se acabar no dia nao conta
//			+ " )) as free_rooms from HotelRoom r where r.quantity > ( "
//				+ "select count(*) as total from Booking b where b.status = 'waiting' and "
//				+ "b.hotelroomid = r.id and "
//				+ ":date >= b.begin AND :date < b.end"	//booking começa antes ou no dia do user input e acaba depois do dia. se acabar no dia nao conta
//			+ " ) ")
//	Iterable<HotelRoom> getFreeRooms(@Param("date") Date date);

//	@Query("select r from HotelRoom r where ")
//	Iterable<HotelRoom> getFreeRooms(@Param("date") Date date);
}


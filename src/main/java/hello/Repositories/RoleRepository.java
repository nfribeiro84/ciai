package hello.Repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.*;

import hello.Models.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	Role findByRoleid(long roleid);
	Role findByName(String name);
}


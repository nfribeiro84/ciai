package hello;

import java.util.Collection;

import hello.Models.User;
import hello.Models.Role;
import hello.Repositories.RoleRepository;
import hello.Repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	private final UserRepository userRepository;

	@Autowired
	public CustomUserDetailsService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("findbyusername");
		System.out.println(username);
		User user = userRepository.findByUsername(username);
		System.out.println(user);
		if (user == null) {
			throw new UsernameNotFoundException(String.format("User %s does not exist!", username));
		}
		return new UserRepositoryUserDetails(user);
	}

	private final static class UserRepositoryUserDetails extends User implements UserDetails {

		@Autowired
		RoleRepository rolesRepository;
		
		private static final long serialVersionUID = 1L;

		private UserRepositoryUserDetails(User user) {
			super(user);
			System.out.println("new userrepositoryuserdetails");
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			System.out.println("getAuthorities");
			//System.out.println(rolesRepository);
			//Role role = rolesRepository.findByRoleid(getRolesid());
			//System.out.println("count: " + rolesRepository.count());
			//System.out.println(rolesRepository.findAll());
			return (Collection<? extends GrantedAuthority>) getRoles();
		}

		@Override
		public String getUsername() {
			return getLogin();
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}

	}

}

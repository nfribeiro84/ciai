package hello.helpers;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Uteis{
	
	public static boolean isStringEmpty(String value)
	{
		if(value == null)
			return true;
		
		if(value.equals(""))
			return true;
		
		return false;
	}
	
	public static Date parseDateToTimestamp(String date) throws ParseException 
	{
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date result =  df.parse(date);  
		return result;  
	}
	
}
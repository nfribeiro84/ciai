package hello.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import hello.Models.Comment;
import hello.Models.Thread;
import hello.Params.Message;
import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;

import hello.Repositories.*;

/*
 * Mapping
 * GET  /threads                  - returns a list of all threads
 * GET  /threads/{id}             - returns the thread with identifier {id}
 * POST /threads                  - creates a new thread or updates an existing thread; returns the thread created/updated
 * DELETE  /threads/{id}          - deletes the thread identifyed by {id} 
 * GET  /threads/{id}/comments
 */

@Controller
@RequestMapping(value="/threads")
public class ThreadController {

    @Autowired
    ThreadRepository threads;
    
    @Autowired
    CommentRepository comments;

		// GET  /threads 			- returns a list of all threads
    @RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Thread> indexJSON() {
        return threads.findAll();
    }
    
    // GET  /threads/{id}		 - returns the thread with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Thread showJSON(@PathVariable("id") long id) throws NotFoundException {
    	Thread thread = threads.findOne(id);
    	if( thread == null )
    		throw new NotFoundException("Not found thread with id " + id);
    	return thread;
    }


    // POST /threads         - creates a new thread or updates an existing thread; returns the thread created/updated
	@RequestMapping(method=RequestMethod.POST, produces={"text/plain","application/json"})
	public @ResponseBody Thread saveIt(@RequestBody Thread thread) 
	{
		return threads.save(thread);
	}

    // Delete /threads/{id}          - deletes the thread identified by {id}
    @RequestMapping(value="{id}", method=RequestMethod.DELETE, produces={"text/plain", "application/json"})
    public @ResponseBody Message deleteThread(@PathVariable("id") long id)
    {
        if(threads.exists(id))
        {
            threads.delete(id);
            return new Message("Thread removido com sucesso");
        }
        else
            return new Message("Não foi encontrado o thread indicado");
    }


    // GET  /threads/{id}/comments      - the comments of a given thread with identifier {id}
    @ResponseBody
    @RequestMapping(value="{id}/comments", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public Iterable<Comment> getCommentsOfThread(@PathVariable("id") long id) throws NotFoundException
    {
        hello.Models.Thread thread = threads.findOne(id);
        if( thread == null )
            throw new NotFoundException("Not found thread with id " + id);

        return comments.findByThreadid(id);
    }

}
package hello.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import hello.Models.*;
import hello.Params.*;
import hello.Repositories.*;
import hello.helpers.*;

/*
 * Mapping
 * GET  /users 			        - returns a list of all users
 * GET  /users/{id} 	       	- returns the hotel with identifier {id}
 * POST /users         	        - creates a new hotel or updates an existing hotel; returns the hotel created/updated
 * DELETE  /users/{id}	     	- deletes the hotel identifyed by {id} 
 * POST /users/login            - check if the user can perform login and replys the user data
 * POST /users/registration     - registers a new user in the application and returns the user data
 */

@Controller
@RequestMapping(value="/users")
public class UserController {

    @Autowired
    UserRepository users;

		// GET  /users.json 			- the list of users
    @RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<User> indexJSON() {
        return users.findAll();
    }
    
    // GET  /users/{id} 		- the hotel with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody User showJSON(@PathVariable("id") long id) throws NotFoundException {
    	User user = users.findOne(id);
    	if( user == null )
    		throw new NotFoundException("Not found user with id " + id);
    	return user;
    }

    // POST /users         - creates a new hotel
	@RequestMapping(method=RequestMethod.POST, produces={"text/plain","application/json"})
	public @ResponseBody User saveIt(@RequestBody User user) 
	{
		return users.save(user);
	}
	
	// Delete /users/{id}			- deletes the user identified by 1
    @RequestMapping(value="{id}", method=RequestMethod.DELETE, produces={"text/plain", "application/json"})
    public @ResponseBody ResponseEntity<String> deleteUSer(@PathVariable("id") long id)
    {
    	if(users.exists(id))
		{
			users.delete(id);
			return new ResponseEntity<String>("Utilizador removido com sucesso", HttpStatus.OK);
		}
    	else
    		return new ResponseEntity<String>("Não foi encontrado o utilizador indicado", HttpStatus.BAD_REQUEST);
    }
    
    // GET /users/username/{username}		- gets an user's data given is username
    @RequestMapping(value="username/{username}", method=RequestMethod.GET, produces={"text/plain", "application/json"})
    public @ResponseBody ResponseEntity<?> findByUsername(@PathVariable("username") String username)
    {
    	User user = users.findByUsername(username);
    	if(user != null)
    	{
    		user.setPassword("");
    		return new ResponseEntity<User>(users.findByUsername(username), HttpStatus.OK);
    	}
    	return new ResponseEntity<Message>(new Message("Utilizador desconhecido na aplicação"), HttpStatus.BAD_REQUEST);
    }

    // POST /users/login				- check if the user can perform login and replys the user data
    @RequestMapping(value="login", method=RequestMethod.POST, produces={"text/plain", "application/json"})
    public @ResponseBody ResponseEntity<?> login(@RequestBody LoginData logindata)
    {
        if(logindata == null || Uteis.isStringEmpty(logindata.getPassword()) || Uteis.isStringEmpty(logindata.getUsername()))
            return new ResponseEntity<Message>(new Message("O username e a password são obrigatórias"), HttpStatus.BAD_REQUEST);

        User user = users.findByUsername(logindata.getUsername());

        if(user == null)
            return new ResponseEntity<Message>(new Message("Utilizador desconhecido na aplicação"), HttpStatus.BAD_REQUEST);
        
        if(!user.getPassword().equals(logindata.getPassword()))
        	return new ResponseEntity<Message>(new Message("Utilizador ou password errados"), HttpStatus.BAD_REQUEST);
        
        user.setPassword(null);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }


	// POST /users/registration			- performs the register of a new user
    @RequestMapping(value="registration", method=RequestMethod.POST, produces={"text/plain", "application/json"})
    @ResponseBody
    public ResponseEntity<?> registration(@RequestBody User newuser)
    {
        String valid = newuser.isValid();
        if(!valid.equals("ok"))
            return new ResponseEntity<Message>(new Message("Faltam parâmetros: " + valid), HttpStatus.BAD_REQUEST);

        if(users.findByUsername(newuser.getUsername()) != null)
            return new ResponseEntity<Message>(new Message("O username escolhido já se encontra em uso"), HttpStatus.BAD_REQUEST);
       /* 
        if(Uteis.isStringEmpty(newuser.getRole()))
        		newuser.setRole("guest");
*/
        return new ResponseEntity<User>(users.save(newuser), HttpStatus.OK);
    }

}
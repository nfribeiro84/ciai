package hello.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import hello.Models.Comment;
import hello.Models.Hotel;
import hello.Models.Thread;
import hello.Params.Message;
import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;

import hello.Models.Comment;
import hello.Models.User;
import hello.Repositories.CommentRepository;
import hello.Repositories.ThreadRepository;
import hello.Repositories.UserRepository;
import hello.Repositories.HotelRepository;

/*
 * Mapping
 * GET  /comments             - returns a list of all comments
 * GET  /comments/{id}        - returns the comment with identifier {id}
 * POST /comments             - creates a new comment or updates an existing comment; returns the comment created/updated
 * POST /comments/hotel/{id}  - creates a new thread
 * DELETE  /comments/{id}     - deletes the comment identifyed by {id} 
 * GET  /comments/{id}/user      - the comment user
 * GET  /comments/status/{status}
 */

@Controller
@RequestMapping(value="/comments")
public class CommentController {

    @Autowired
    CommentRepository comments;

    @Autowired
    ThreadRepository threads;

    @Autowired
    HotelRepository hotels;

    @Autowired
    UserRepository users;

		// GET  /comments 			- the list of comments
    @RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Comment> indexJSON() {
        return comments.findAll();
    }
    
    // GET  /comments/{id} 		- the comment with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Comment showJSON(@PathVariable("id") long id) throws NotFoundException {
    	Comment comment = comments.findOne(id);
    	if( comment == null )
    		throw new NotFoundException("Not found comment with id " + id);
    	return comment;
    }


    // POST /comments         - creates a new comment
    @RequestMapping(method=RequestMethod.POST, produces={"text/plain","application/json"})
    public @ResponseBody Comment saveIt(@RequestBody Comment comment) 
    {
        return comments.save(comment);
    }


    // POST /comments/hotel/{id}         - creates a new thread on hotel with given id
	@RequestMapping(value="hotel/{id}", method=RequestMethod.POST, produces={"text/plain","application/json"})
	public @ResponseBody Comment saveFirst(@PathVariable("id") long id, @RequestBody Comment comment) throws NotFoundException 
	{
        Hotel hotel = hotels.findOne(id);
        System.out.println(hotel);
        if(hotel == null)
            throw new NotFoundException("Not found hotel with id " + id);

        Thread thread = new Thread(0, "", id);

        threads.save(thread);

        comment.setThreadid(thread.getId());
		  
        return comments.save(comment);

	}

    // Delete /comments/{id}          - deletes the comment identified by 1
    @RequestMapping(value="{id}", method=RequestMethod.DELETE, produces={"text/plain", "application/json"})
    public @ResponseBody Message deleteComment(@PathVariable("id") long id)
    {
        if(comments.exists(id))
        {
            comments.delete(id);
            return new Message("Comentario removido com sucesso");
        }
        else
            return new Message("Não foi encontrado o Comentario indicado");
    }



    // GET  /comments/{id}/user      - the comment user
    @ResponseBody
    @RequestMapping(value="{id}/user", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public User getCommentUser(@PathVariable("id") long id) throws NotFoundException
    {
        Comment comment = comments.findOne(id);
        if( comment == null )
            throw new NotFoundException("Not found comment with id " + id);

        return users.findOne(comment.getUserid());
    }

    
    // GET  /comments/status/{status}      - the comment with status {status}
    @RequestMapping(value="status/{status}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Comment> showbyStatus(@PathVariable("status") String status) {
        
        return comments.findByStatus(status);
    }

}
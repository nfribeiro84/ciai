package hello.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import hello.Models.Booking;
import hello.Models.HotelRoom;
import hello.Params.Message;
import javassist.NotFoundException;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;

import hello.Repositories.*;

/*
 * Mapping
 * GET  /hotelrooms             - returns a list of all hotel rooms
 * GET  /hotelrooms/{id}        - returns the hotel room with identifier {id}
 * POST /hotelrooms             - creates a new hotel room or updates an existing hotel room; returns the hotel room created/updated
 * DELETE  /hotelrooms/{id}     - deletes the hotel room identifyed by {id} 
 * GET  /hotelrooms/hotel/{hotelid}             - the list of hotel rooms
 * GET  /hotelrooms/hotel/{hotelid}/roomtype/{roomtypeid} - the list of hotel rooms of type x
 */

@Controller
@RequestMapping(value="/hotelrooms")
public class HotelRoomController {

    @Autowired
    HotelRoomRepository hotel_rooms;

    @Autowired
    BookingRepository bookings;

		// GET  /hotelrooms 			- the list of hotel rooms
    @RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<HotelRoom> indexJSON() {
        return hotel_rooms.findAll();
    }
    
    // GET  /hotelrooms/{id} 		     - the hotel room with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody HotelRoom showJSON(@PathVariable("id") long id) throws NotFoundException {
    	HotelRoom hotel_room = hotel_rooms.findOne(id);
    	if( hotel_room == null )
    		throw new NotFoundException("Not found hotel_room with id " + id);
    	return hotel_room;
    }


    // POST /hotelrooms         - creates a new hotel room
	@RequestMapping(method=RequestMethod.POST, produces={"text/plain","application/json"})
	public @ResponseBody HotelRoom saveIt(@RequestBody HotelRoom hotel_room) 
	{
		return hotel_rooms.save(hotel_room);
	}

    // Delete /hotelrooms/{id}          - deletes the hotel room identified by 1
    @RequestMapping(value="{id}", method=RequestMethod.DELETE, produces={"text/plain", "application/json"})
    public @ResponseBody Message deleteHotelRoom(@PathVariable("id") long id)
    {
        if(hotel_rooms.exists(id))
        {
            hotel_rooms.delete(id);
            return new Message("Hotel Room removido com sucesso");
        }
        else
            return new Message("Não foi encontrado o Hotel Room indicado");
    }

    // GET  /hotelrooms/hotel/{hotelid}             - the list of hotel rooms
    @RequestMapping(value="hotel/{hotelid}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<HotelRoom> showbyhotelid(@PathVariable("hotelid") long hotelid) {
        
        return hotel_rooms.findByHotelid(hotelid);
    }

    // GET  /hotelrooms/hotel/{hotelid}/roomtype/{roomtypeid} - the list of hotel rooms
    @RequestMapping(value="hotel/{hotelid}/roomtype/{roomtypeid}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<HotelRoom> showByHotelidAndTypeid(@PathVariable("hotelid") long hotelid, @PathVariable("roomtypeid") long roomtypeid) {
        
        return hotel_rooms.findByHotelidAndRoomtypeid(hotelid, roomtypeid);
    }

    // GET  /hotelrooms/free/{date} - the list of free hotel rooms for this date
    // e.g. /hotelrooms/free/2015-01-01.json
    @RequestMapping(value="free/{date}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<HotelRoom> showFreeRooms(@PathVariable("date") String date) throws ParseException {
    	
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        java.util.Date result =  df.parse(date);  
    	System.out.println("time: " + new Date(result.getTime()));
    	
    	//get hotel rooms
    	Iterable<HotelRoom> hotelrooms = hotel_rooms.findAll();
    	
    	ArrayList<HotelRoom> free_rooms = new ArrayList<HotelRoom>();
    	
    	for(HotelRoom hr : hotelrooms) {
    		ArrayList<Booking> books = (ArrayList<Booking>) bookings.getBookingsByDateAndHotelroomidAndStatus(new Date(result.getTime()), hr.getId(), "accepted");

			System.out.println("Hotelid: "+hr.getHotelid()+", HotelRoomid: "+hr.getId()+", books: "+books.size());
			
			if(hr.getQuantity() > books.size()) {
				hr.setAvailable(hr.getQuantity() - books.size());
				free_rooms.add(hr);
			}
    	}
    	
    	return free_rooms;
    }

    // GET  /hotelrooms/hotel/{hotelid}/free/{date} - the list of free hotel rooms for this date
    // e.g. /hotelrooms/hotel/1/free/2014-12-29.json
    @RequestMapping(value="hotel/{hotelid}/free/{date}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<HotelRoom> showFreeRoomsByHotel(@PathVariable("hotelid") long hotelid, @PathVariable("date") String date) throws ParseException {
    	
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        java.util.Date result =  df.parse(date);  
    	System.out.println("time: " + new Date(result.getTime()));
    	
    	//get hotel rooms
    	Iterable<HotelRoom> hotelrooms = hotel_rooms.findByHotelid(hotelid);
    	
    	ArrayList<HotelRoom> free_rooms = new ArrayList<HotelRoom>();
    	
    	for(HotelRoom hr : hotelrooms) {
    		ArrayList<Booking> books = (ArrayList<Booking>) bookings.getBookingsByDateAndHotelroomidAndStatus(new Date(result.getTime()), hr.getId(), "accepted");

			System.out.println("Hotelid: "+hr.getHotelid()+", HotelRoomid: "+hr.getId()+", books: "+books.size());
			
			hr.setAvailable(hr.getQuantity() - books.size());
			free_rooms.add(hr);
    	}
    	
    	return free_rooms;
    }


}
















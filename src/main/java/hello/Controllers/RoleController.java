package hello.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.beans.factory.annotation.Autowired;

import hello.Models.Role;
import hello.Repositories.RoleRepository;

/*
 * Mapping
 * GET  /comments             - returns a list of all comments
 * GET  /comments/{id}        - returns the comment with identifier {id}
 * POST /comments             - creates a new comment or updates an existing comment; returns the comment created/updated
 * POST /comments/hotel/{id}  - creates a new thread
 * DELETE  /comments/{id}     - deletes the comment identifyed by {id} 
 * GET  /comments/{id}/user      - the comment user
 * GET  /comments/status/{status}
 */

@Controller
@RequestMapping(value="/roles")
public class RoleController {

    @Autowired
    RoleRepository roles;

	// GET  /roles 			- the list of roles
    @RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Role> indexJSON() {
        return roles.findAll();
    }
    
    
}
package hello.Controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import hello.Models.Booking;
import hello.Params.Message;
import hello.Repositories.BookingRepository;
import javassist.NotFoundException;

/*
 * Mapping
 * GET  /bookings             - returns a list of all bookings
 * GET  /bookings/{id}        - returns the booking with identifier {id}
 * POST /bookings             - creates a new booking or updates an existing booking; returns the booking created/updated
 * DELETE  /bookings/{id}     - deletes the booking identifyed by {id} 
 * GET  /bookings/user/{id}      - the bookings of user with identifier {id}
 * GET  /bookings/hotelroom/{id}      - the booking with identifier {id}
 * GET  /bookings/status/{status}      - the booking with status {status}
 */

@Controller
@RequestMapping(value="/bookings")
public class BookingController {

    @Autowired
    BookingRepository bookings;

	// GET  /bookings 			- the list of bookings
    @RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Booking> indexJSON() {
        return bookings.findAll();
//        for(Booking room : bookings.getBookingsByOwner((long)1)) 
//        	System.out.println(room.getHotelRoom());
//        
//        return bookings.getBookingsByOwner((long)1);
    }
    
    // GET  /bookings/{id} 		- the booking with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Booking showJSON(@PathVariable("id") long id) throws NotFoundException {
    	Booking booking = bookings.findOne(id);
    	if( booking == null )
    		throw new NotFoundException("Not found booking with id " + id);
    	return booking;
    }


    // POST /bookings         - creates a new booking
	@RequestMapping(method=RequestMethod.POST, produces={"text/plain","application/json"})
	public @ResponseBody Booking saveIt(@RequestBody Booking booking) 
	{
		return bookings.save(booking);
	}


    // Delete /bookings/{id}          - deletes the booking identified by 1
    @RequestMapping(value="{id}", method=RequestMethod.DELETE, produces={"text/plain", "application/json"})
    public @ResponseBody Message deleteBooking(@PathVariable("id") long id)
    {
        if(bookings.exists(id))
        {
            bookings.delete(id);
            return new Message("Reserva removida com sucesso");
        }
        else
            return new Message("Não foi encontrado a Reserva indicada");
    }
    

    // GET  /bookings/user/{id}      - the bookings of user with identifier {id}
    @RequestMapping(value="user/{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Booking> showbyUser(@PathVariable("id") long id) {
        
        return bookings.findByUserid(id);
    }

    // GET  /bookings/hotelroom/{id}      - the booking with identifier {id}
    @RequestMapping(value="hotelroom/{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Booking> showbyhotel(@PathVariable("id") long id) {
        
        return bookings.findByHotelroomid(id);
    }

    // GET  /bookings/status/{status}      - the booking with status {status}
    @RequestMapping(value="status/{status}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Booking> showbystatus(@PathVariable("status") String status) {
        
        return bookings.findByStatus(status);
    }
    
    // GET  /bookings/date/{begin}      - the booking with identifier {id}
    // e.g. bookings/date/2015-01-01.json
    @RequestMapping(value="date/{date}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Booking> showbydate(@PathVariable("date") String date) throws ParseException {
    		
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        java.util.Date result =  df.parse(date);
    	System.out.println(new Date(result.getTime()));  
        return bookings.getBookingsByDateAndStatus(new Date(result.getTime()), "accepted");
    }
    
    
    // GET  /bookings/{id}      - the booking with identifier {id}
    // GET  /bookings/{id}      - the booking with identifier {id}
    // GET  /bookings/{id}      - the booking with identifier {id}


}









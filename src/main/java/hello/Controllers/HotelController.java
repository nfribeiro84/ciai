package hello.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import hello.Exceptions.HotelNotFoundException;
import hello.Models.Booking;
import hello.Models.Hotel;
import hello.Models.HotelRoom;
import hello.Models.Role;
import hello.Models.User;
import hello.Params.*;
import hello.Repositories.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.security.PermitAll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;

/*
 * Mapping
 * GET  /hotels 			- returns a list of all hotels
 * GET  /hotels/{id} 		- returns the hotel with identifier {id}
 * POST /hotels         	- creates a new hotel or updates an existing hotel; returns the hotel created/updated
 * DELETE  /hotels/{id}     - deletes the hotel identifyed by {id} 
 * GET  /hotels/owner/{ownerid}
 * GET  /hotels/{id}/threads
 * GET  /hotels/status/{status}      - the hotels with status {status}
 */

@Controller
@RequestMapping(value="/hotels")
public class HotelController {
	
	@Autowired
    HotelRepository hotels;

    @Autowired
    ThreadRepository threads;

    @Autowired
    BookingRepository bookings;

    @Autowired
    HotelRoomRepository hotelrooms;
    
    @Autowired
    RoleRepository roles;

	// GET  /hotels 			- returns a list of all hotels
    @PermitAll
    @RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Hotel> indexJSON() {
    	return hotels.findAll();
    }
    
    // GET  /hotels/{id} 		- the hotel with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Hotel showJSON(@PathVariable("id") long id) {
    	Hotel hotel = hotels.findOne(id);
    	if( hotel == null )
    		throw new HotelNotFoundException();
    	return hotel;
    }
    
    // POST /hotels         	- creates a new hotel
    @RequestMapping(method=RequestMethod.POST, produces={"text/plain","application/json","*/*"})
    public @ResponseBody ResponseEntity<?> saveIt(@RequestBody Hotel hotel, @AuthenticationPrincipal User loggedUser) 
    {
    	Role owner = roles.findByName("Owner");
    	if(loggedUser.getRoles().contains(owner))
    	{
    		if(loggedUser.getId() == hotel.getOwnerid())
    			return new ResponseEntity<Hotel>(hotels.save(hotel), HttpStatus.OK);
    		else
    			return new ResponseEntity<Message>(new Message("Só pode criar hoteis para a sua conta"), HttpStatus.FORBIDDEN);
    	}
    	else
    		return new ResponseEntity<Message>(new Message("Não tem permissões para guardar um hotel"), HttpStatus.FORBIDDEN);
    }
 
    // Delete /hotels/{id}			- deletes the hotel identified by 1
    @RequestMapping(value="{id}", method=RequestMethod.DELETE, produces={"text/plain", "application/json"})
    public @ResponseBody Message deleteHotel(@PathVariable("id") long id)
    {
    	if(hotels.exists(id))
		{
			hotels.delete(id);
			return new Message("Hotel removido com sucesso");
		}
    	else
    		return new Message("Não foi encontrado o hotel indicado");
    }


    // GET  /hotels/owner/{ownerid}
    @ResponseBody
    @RequestMapping(value="owner/{ownerid}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public Iterable<Hotel> getHotelsOfOwner(@PathVariable("ownerid") long ownerid, @AuthenticationPrincipal User loggedUser)
    {
    	System.out.println("owner id " + ownerid);
        return hotels.findByOwnerid(ownerid);
    }


    // GET  /hotels/{id}/threads      - the threads of a given hotel
    @ResponseBody
    @RequestMapping(value="{id}/threads", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public Iterable<hello.Models.Thread> getThreadsOfHotel(@PathVariable("id") long id)
    {
    	System.out.println("hotel id " + id);
        return threads.findByHotelid(id);
    }

    
    // GET  /hotels/status/{status}      - the hotels with status {status}
    @RequestMapping(value="status/{status}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Hotel> showbyStatus(@PathVariable("status") String status) {
        return hotels.findByStatus(status);
    }

    @RequestMapping(value="waitingbookings/{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody HotelWaitingBookings getWaitingBookings(@PathVariable("id") long hotelid)
    {
    	HotelWaitingBookings list = new HotelWaitingBookings();
    	System.out.println("waitingbookings " + hotelid);
        for(HotelRoom hotelroom : hotelrooms.findByHotelid(hotelid))
        {
        	System.out.println("     hotelroom " + hotelroom.getId());
        	for(Booking booking : bookings.findByHotelroomid(hotelroom.getId()))
        		if(booking.getStatus().equals("waiting"))
        				list.pushId(booking.getId());
        }
        return list;
    }


/*
    @RequestMapping(value="available", method=RequestMethod.POST, produces={"text/plain","application/json"})
    public @ResponseBody List<HotelAvailable> showAvailable(@RequestBody DateRange daterange)
    {	
    	List<HotelAvailable> result = new ArrayList<HotelAvailable>();
    	for(Hotel hotel : hotels.findAll())
    	{
    		HotelAvailable hotelavailable = new HotelAvailable(hotel.getId(), hotel.getName());
    		for(HotelRoom room : rooms.findByHotelid(hotelavailable.getHotelid()))
    		{
    			int freerooms = room.getQuantity();
    			for(Booking booking : bookings.findByHotelroomid(room.getId()))
    			{
    				if(daterange.getBegin().getTime() < booking.getEnd().getTime() && daterange.getEnd().getTime() > booking.getBegin().getTime())
    					if(booking.getStatus().equals("accepted"))
    						freerooms--;
    			}
    			if(freerooms > 0)
    			{
    				RoomAvailable roomavailable = new RoomAvailable(room.getId(), freerooms,room.getPrice());
    				hotelavailable.addRoomAvailable(roomavailable);
    			}
    		}
    		if(hotelavailable.getCountRooms() > 0)
    			result.add(hotelavailable);
    	}
    	
    	return result;
    }*/
}








package hello.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import hello.Models.RoomType;
import hello.Params.Message;
import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;

import hello.Repositories.*;

/*
 * Mapping
 * GET  /roomtypes             - returns a list of all room_types
 * GET  /roomtypes/{id}        - returns the room_type with identifier {id}
 * POST /roomtypes             - creates a new room_type or updates an existing room_type; returns the room_type created/updated
 * DELETE  /roomtypes/{id}     - deletes the room_type identifyed by {id} 
 */

@Controller
@RequestMapping(value="/roomtypes")
public class RoomTypeController {

    @Autowired
    RoomTypeRepository room_types;

		// GET  /roomtypes 			- the list of room_types
    @RequestMapping(method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<RoomType> indexJSON() {
        return room_types.findAll();
    }
    
    // GET  /roomtypes/{id}     		- the room_type with identifier {id}
    @RequestMapping(value="{id}", method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody RoomType showJSON(@PathVariable("id") long id) throws NotFoundException {
    	RoomType room_type = room_types.findOne(id);
    	if( room_type == null )
    		throw new NotFoundException("Not found room_type with id " + id);
    	return room_type;
    }

    // POST /roomtypes             - creates a new room_type
	@RequestMapping(method=RequestMethod.POST, produces={"text/plain","application/json"})
	public @ResponseBody RoomType saveIt(@RequestBody RoomType room_type) 
	{
		return room_types.save(room_type);
	}

    // Delete /roomtypes/{id}          - deletes the room_type identified by 1
    @RequestMapping(value="{id}", method=RequestMethod.DELETE, produces={"text/plain", "application/json"})
    public @ResponseBody Message deleteRoom_type(@PathVariable("id") long id)
    {
        if(room_types.exists(id))
        {
            room_types.delete(id);
            return new Message("Room_type removido com sucesso");
        }
        else
            return new Message("Não foi encontrado o Room_type indicado");
    }
}
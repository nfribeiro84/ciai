package hello;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;

import java.sql.Timestamp;

import hello.Models.*;
import hello.Models.Thread;
import hello.Repositories.*;
import hello.helpers.Uteis;


@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);
	
	/**
	 * The main() method uses Spring Boot’s SpringApplication.run() method to launch an application.
	 * The run() method returns an ApplicationContext where all the beans that were created 
	 * either by your app or automatically added thanks to Spring Boot are.
	 * @param args
	 */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
        
    @Autowired
    HotelRepository hotels;
    
    @Autowired
    UserRepository users;
    
    @Autowired
    ThreadRepository threads;
    
    @Autowired
    CommentRepository comments;
    
    @Autowired
    RoomTypeRepository roomtypes;
    
    @Autowired
    HotelRoomRepository rooms;
    
    @Autowired
    BookingRepository bookings;
    
    @Autowired 
    RoleRepository roles;
    
    @Override
    public void run(String... strings) throws ParseException {
    	 
    	log.info("Setting up seed data");
     
    	roles.deleteAll();
    	
    	Role myRoles[] = {
    			new Role(1, "Owner"),
    			new Role(2, "Moderator"),
    			new Role(3, "Guest")
    	};
    	
    	for(Role role : myRoles)
    	{
    		roles.save(role);
    	}
    	
    	Role owner = roles.findOne(1l);
    	
        users.deleteAll();
       	User myUsers[] = {
       			new User(1,"Dono 1", "owner1", "test", owner), 
       			new User(2,"Dono 2", "owner2", "test", owner), 
       			new User(3,"Dono 3", "owner3", "test", owner), 
				new User(4,"Dono 4", "owner4", "test", owner),
				new User(5,"Moderador 1", "moderator1", "test", roles.findOne(2l)),
				new User(6,"Cliente 1", "client1", "test", roles.findOne(3l))
		};
       	
        for(User user : myUsers) {
            users.save(user);
        }
    	
        hotels.deleteAll();
        Hotel myHotels[] = {new Hotel(1,"Marriot", 1, "Rua da flores", "Hostel", 3), 
                         new Hotel(2,"Intercontinental", 1, "Rua da flores", "Hostel", 3), 
                         new Hotel(3,"Trip", 1, "Rua da flores", "Hostel", 3), 
                         new Hotel(4,"Holiday Inn", 2, "Rua da flores", "Hostel", 3), 
                         new Hotel(5,"Tulip", 2, "Rua da flores", "Hostel", 3), 
                         new Hotel(6,"Hostel da Costa", 3, "Rua da flores", "Hostel", 3)};
     
     
        for(Hotel hotel : myHotels) hotels.save(hotel);
    	
        threads.deleteAll();
        Thread myThreads[] = {new Thread(1,"Marriot Thread", 1)};
     
     
        for(Thread thread : myThreads) threads.save(thread);
        
        comments.deleteAll();
        Date date= new Date(new java.util.Date().getTime());
        Comment myComm[] = {
                new Comment(1,"Muito podre", "accepted", date, true, 1,1),
                new Comment(2,"Adorei", "waiting", date, false, 2,1)
                };
     
     
        for(Comment comm : myComm) comments.save(comm);

            // Room Types
        
        roomtypes.deleteAll();
        RoomType myType[] = {
                new RoomType(1,"Suite privada", 2),
                new RoomType(2,"Camarata", 8),
                new RoomType(3,"Quarto Simples",1),
                new RoomType(4,"Quarto Duplo", 4)
                };
     
     
        for(RoomType type : myType) roomtypes.save(type);

            // Hotel Rooms
        
        rooms.deleteAll();
        HotelRoom myRoom[] = {
                new HotelRoom(1, 1, 30.0, 1, 1, "vista para o mar"),
                new HotelRoom(2, 1, 15.0, 2, 1, "comm beliches"),
                new HotelRoom(3, 1, 40.0, 1, 2, "vista para o mar"),
                new HotelRoom(4, 1, 18.0, 2, 2, "vista para o mar"),
                new HotelRoom(5, 1, 20.0, 1, 3, "vista para o mar"),
                new HotelRoom(6, 1, 15.0, 2, 4, "vista para o mar")
                };
     
     
        for(HotelRoom room : myRoom) rooms.save(room);

            // Bookings
        
        bookings.deleteAll();
       

    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        java.util.Date result_ =  df.parse("2015-11-26");  
        java.util.Date result_before_ =  df.parse("2015-11-23");  

        Date result = new java.sql.Date(result_.getTime());
        Date result_before = new java.sql.Date(result_before_.getTime());
        

        Booking myBook[] = {
                new Booking(1, "", date, result_before, result, 5, 6, 1, 30),
                new Booking(2, "", date, result_before, result, 5, 6, 2, 30),
                new Booking(3, "", date, result_before, result, 5, 6, 3, 30),
                new Booking(4, "", date, result_before, result, 5, 6, 4, 30),
                new Booking(5, "", date, result_before, result, 5, 6, 5, 30),
        		new Booking(6, "", date, result_before, result, 5, 6, 6, 30)
        		};
     
     
        for(Booking room : myBook) {
        	room.setStatus("accepted");
        	bookings.save(room);
        }
    }

}



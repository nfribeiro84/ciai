package hello.Params;

public class RoomAvailable{
	private long hotelroomid;
	private int available;
	private double price;

	protected RoomAvailable() {};

	public RoomAvailable(long hotelroomid, int available, double price)
	{
		this.hotelroomid = hotelroomid;
		this.available = available;
		this.price = price;
	}

	public long getHotelroomid(){
		return hotelroomid;
	}

	public void setHotelroomid(long hotelroomid){
		this.hotelroomid = hotelroomid;
	}

	public int getAvailable(){
		return available;
	}

	public void setAvailable(int available){
		this.available = available;
	}

	public double getPrice(){
		return price;
	}

	public void setPrice(double price){
		this.price = price;
	}
}

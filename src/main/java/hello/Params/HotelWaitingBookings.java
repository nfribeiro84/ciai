package hello.Params;

import java.util.*;

public class HotelWaitingBookings {
	
	private List<Long> ids;

	public HotelWaitingBookings() { ids = new ArrayList<>();};
	
	public HotelWaitingBookings(List<Long> ids){
		this.ids = ids;
	}
	
	public void setIds(List<Long> ids){
		this.ids = ids;
	}
	
	public List<Long> getIds(){
		return ids;
	}
	
	public void pushId(long id)
	{
		ids.add(id);
	}
	
}

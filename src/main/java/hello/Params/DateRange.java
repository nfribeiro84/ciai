package hello.Params;

import java.util.Date;


public class DateRange{
	private Date begin;
	private Date end;

	protected DateRange() {}
    
    public DateRange(Date begin, Date end)
	{
		this.begin = begin;
		this.end = end;
	}

	public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}


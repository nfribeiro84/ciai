package hello.Params;

import java.util.*;


public class HotelAvailable{
	private long hotelid;
	private String name;
	private List<RoomAvailable> roomsavailable;

	protected HotelAvailable() {};

	public HotelAvailable(long hotelid, String name)
	{
		this.hotelid = hotelid;
		this.name = name;
		this.roomsavailable = new ArrayList<RoomAvailable>();
	}

	public long getHotelid(){
		return hotelid;
	}

	public void setHotelid(long hotelid){
		this.hotelid = hotelid;
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public Iterable<RoomAvailable> getRoomsavailable(){
		return roomsavailable;
	}

	public void setRoomsavailable(ArrayList<RoomAvailable> roomsavailable){
		this.roomsavailable = roomsavailable;
	}

	public void addRoomAvailable(RoomAvailable room)
	{
		roomsavailable.add(room);
	}
	
	public int getCountRooms()
	{
		return roomsavailable.size();
	}
}


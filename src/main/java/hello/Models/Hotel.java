package hello.Models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Hotel {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String name;    
    private String address;    
    private String category;    
    private int rating;    
    private String status; // active, inactive, waiting
    private long ownerid;

	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn( name = "ownerid", insertable = false, updatable = false )
	private User owner;
 
    protected Hotel() {}
    
    public Hotel(long id, String name, long ownerid, String address, String category, int rating) {
    	this.id = id;
    	this.name = name;
        this.ownerid = ownerid;
        this.status = "waiting";
        this.address = address;
        this.category = category;
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public long getOwnerid() {
        return ownerid;
    }

    public void setOwnerid(long ownerid) {
        this.ownerid = ownerid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getOwner() {
        return owner;
    }
    
    @Override
    public String toString() {
    	return "Id: " + getId() + "\nName: " + getName() + "\n ownerId: " + getOwnerid();
    }

}


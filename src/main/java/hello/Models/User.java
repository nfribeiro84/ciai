package hello.Models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnore;

import hello.Repositories.RoleRepository;


@Entity
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String name;
    private String username;
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "role_id") })	
    private Set<Role> roles = new HashSet<Role>();
 
 
    protected User() {}
    
    public User(User user) {
		super();
		this.id = user.getId();
		this.name = user.getName();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.roles = user.getRoles();
	}
    
    public User(long id, String name, String username, String password, Role role) 
    {
    	this.id = id;
    	this.name = name;
        this.username = username;
        this.password = password;
        this.roles.add(role);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }
    
    public String getLogin(){
    	return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
//    public int getRolesid()
//    {
//    	return rolesid;
//    }
//    
//    public void setRolesid(int rolesid)
//    {
//    	this.rolesid = rolesid;
//    }
    
    public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

    @Override
    public String toString() {
    	return "Id: " + getId() + "\nName: " + getName() + "\nRole: " + getRoles();
    	
    }


    public String isValid()
    {
        String result = "";

        if(isEmpty(this.username))
            result = "username ";

        if(isEmpty(this.password))
            result += "password ";

        if(isEmpty(this.name))
            result += "name";

        if(result.equals(""))
            return "ok";
        else return result;

    }

    private boolean isEmpty(String value)
    {
        if(value == null)
            return true;
        if(value.equals(""))
            return true;
        return false;
    }
}


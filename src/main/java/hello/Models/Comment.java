package hello.Models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String comment;
    private String status;
    private Date created;
    private Boolean isFirst;
    private long userid;
    private long threadid;

	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn( name = "threadid", insertable = false, updatable = false )
	private Thread thread;
 
    protected Comment() {}
    
    public Comment(long id, String comment, String status, Date created, Boolean isFirst, long userid, long threadid) {
    	this.id = id;
    	this.comment = comment;
    	this.status = status;
    	this.created = created;
        this.isFirst = isFirst;
    	this.userid = userid;
    	this.threadid = threadid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Boolean getIsFirst() {
        return this.isFirst;
    }

    public void setIsFirst(Boolean isFirst) {
        this.isFirst = isFirst;
    }

    public long getUserid() {
        return this.userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public long getThreadid() {
        return this.threadid;
    }

    public void setThreadid(long threadid) {
        this.threadid = threadid;
    }

    public Thread getThread() {
        return this.thread;
    }
    
    @Override
    public String toString() {
    	return "Id: " + getId() + "\nComment: " + getComment();
    }

}


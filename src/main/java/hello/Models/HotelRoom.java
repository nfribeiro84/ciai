package hello.Models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class HotelRoom {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private int quantity;
    private int available;    
    private double price;   
    private String features;

    private long roomtypeid;    
    private long hotelid;    
    
	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn( name = "roomtypeid", insertable = false, updatable = false )
	private RoomType roomtype;    
    
	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn( name = "hotelid", insertable = false, updatable = false )
	private Hotel hotel;
 
    protected HotelRoom() {}
    
    public HotelRoom(long id, int quantity, double price, long roomtypeid, long hotelid, String features) {
    	this.id = id;
        this.quantity = quantity;
        this.price = price;
        this.roomtypeid = roomtypeid;
    	this.hotelid = hotelid;
    	this.features = features;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getFeatures(){
        return features;
    }

    public void setFeatures(String features){
        this.features = features;
    }

    public long getRoomtypeid() {
        return roomtypeid;
    }

    public void setRoomtypeid(long roomtypeid) {
        this.roomtypeid = roomtypeid;
    }

    public long getHotelid() {
        return hotelid;
    }

    public void setHotelid(long hotelid) {
        this.hotelid = hotelid;
    }

    public Hotel getHotel() {
        return this.hotel;
    }

    public RoomType getRoomType() {
        return this.roomtype;
    }
    
    @Override
    public String toString() {
    	return "Id: " + getId();
    }

}


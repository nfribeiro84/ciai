package hello.Models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Booking {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String comment;    
    private Date created;    
    private Date begin;    
    private Date end;    
    private int persons;    
    private String status;
    private double price;

    private long userid;    
    private long hotelroomid;    

	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn( name = "hotelroomid", insertable = false, updatable = false )
	private HotelRoom hotelroom;
 
    protected Booking() {}
    
    public Booking(long id, String comment, Date created, Date begin, Date end, int persons, long userid, long hotelroomid, double price) {
    	this.id = id;
        this.comment = comment;
        this.created = created;
        this.begin = begin;
        this.end = end;
        this.persons = persons;
        this.status = "waiting";
        this.userid = userid;
    	this.hotelroomid = hotelroomid;
    	this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = new Date(created.getTime());
    }

    public Date getBegin() {
        return this.begin;
    }

    public void setBegin(Date begin) {
        this.begin = new Date(begin.getTime());
    }

    public Date getEnd() {
        return this.end;
    }

    public void setEnd(Date end) {
        this.end = new Date(end.getTime());
    }

    public int getPersons() {
        return this.persons;
    }

    public void setPersons(int persons) {
        this.persons = persons;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public double getPrice(){
    	return price;
    }
    
    public void setPrice(double price){
    	this.price = price;
    }

    public long getUserid() {
        return this.userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public long getHotelroomid() {
        return this.hotelroomid;
    }

    public void setHotelroomid(long hotelroomid) {
        this.hotelroomid = hotelroomid;
    }

    public HotelRoom getHotelRoom() {
        return this.hotelroom;
    }
    
    @Override
    public String toString() {
    	return "Id: " + getId() + "\nComment: " + getComment();
    }

}


package hello.Models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;

import hello.Repositories.CommentRepository;

@Entity
public class Thread {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String name;    
    private long hotelid;    
//    private Comment comments[];    

	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn( name = "hotelid", insertable = false, updatable = false )
	private Hotel hotel;
 
    protected Thread() {}
    
    public Thread(long id, String name, long hotelid) {
    	this.id = id;
    	this.name = name;
    	this.hotelid = hotelid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getHotelid() {
        return hotelid;
    }

    public void setHotelid(long hotelid) {
        this.hotelid = hotelid;
    }

    public Hotel getHotel() {
        return hotel;
    }
    
    @Override
    public String toString() {
    	return "Id: " + getId() + "\nName: " + getName();
    }

}


package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

@Configuration
public class OAuth2ServerConfiguration {

	private static final String RESOURCE_ID = "restservice";

	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends
			ResourceServerConfigurerAdapter {

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) {
			// @formatter:off
			resources
				.resourceId(RESOURCE_ID);
			// @formatter:on
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {
			// @formatter:off
		
			 http
	          .authorizeRequests()
	          	.antMatchers("/hotelrooms/hotel/*/free/*").permitAll()
	          	.antMatchers("/bookings.json").permitAll()
	          	.antMatchers("/").permitAll()
	            .antMatchers("/views/**", "/assets/**", "/images/**", "/scripts/**", "/js/**").permitAll()
	            .antMatchers("/favicon.ico").permitAll()
	            
	            	.antMatchers(HttpMethod.POST, "/users/registration").permitAll()
	            
	            	//permissões para o HotelController
		            .antMatchers(HttpMethod.GET, "/hotels").permitAll()
		            .antMatchers(HttpMethod.GET, "/hotels/*").permitAll()
		            .antMatchers(HttpMethod.GET, "/hotels/*/threads").permitAll()
		            .antMatchers(HttpMethod.GET, "/roles").permitAll()
		            
		            //permissões para o RoomTypeController
		            .antMatchers(HttpMethod.GET, "/roomtypes").permitAll()
		            
		            //permissões para o HotelRoomController
		            .antMatchers(HttpMethod.GET, "/hotelrooms/hotel/*").permitAll()
		            
		            //permissões para o ThreadsController
		            .antMatchers(HttpMethod.GET, "/threads/*/comments").permitAll()
		            
		            .antMatchers(HttpMethod.GET, "/comments/**").permitAll()
		            
		            
	            .anyRequest().authenticated();
			
			
			/*
			http
				.authorizeRequests()
					.antMatchers("/users").hasRole("ADMIN")
					.antMatchers("/greeting").authenticated();*/
			// @formatter:on
		}

	}

	@Configuration
	@EnableAuthorizationServer
	protected static class AuthorizationServerConfiguration extends
			AuthorizationServerConfigurerAdapter {

		private TokenStore tokenStore = new InMemoryTokenStore();

		@Autowired
		@Qualifier("authenticationManagerBean")
		private AuthenticationManager authenticationManager;

		@Autowired
		private CustomUserDetailsService userDetailsService;

		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints)
				throws Exception {
			// @formatter:off
			endpoints
				.tokenStore(this.tokenStore)
				.authenticationManager(this.authenticationManager)
				.userDetailsService(userDetailsService);
			// @formatter:on
		}

		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
			// @formatter:off
			clients
				.inMemory()
					.withClient("clientapp")
						.authorizedGrantTypes("password", "refresh_token")
						.authorities("USER")
						.scopes("read", "write")
						.resourceIds(RESOURCE_ID)
						.secret("123456")
						.accessTokenValiditySeconds(3600);;
			// @formatter:on
		}

		@Bean
		@Primary
		public DefaultTokenServices tokenServices() {
			DefaultTokenServices tokenServices = new DefaultTokenServices();
			tokenServices.setSupportRefreshToken(true);
			tokenServices.setTokenStore(this.tokenStore);
			return tokenServices;
		}

	}

}

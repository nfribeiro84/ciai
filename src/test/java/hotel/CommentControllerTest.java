package hotel;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.CoreMatchers.containsString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.h2.command.dml.Delete;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;


import hello.Application;
import hello.Controllers.CommentController;
import hello.Controllers.ThreadController;
import hello.Models.Comment;
import hello.Models.Hotel;
import hello.Models.Thread;
import hello.Repositories.CommentRepository;
import hello.Repositories.HotelRepository;
import hello.Repositories.ThreadRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
public class CommentControllerTest {
	
	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;
	
	@Autowired
	CommentRepository comments;

	@Autowired
	ThreadRepository threads;

	@Autowired
	HotelRepository hotels;

	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}


	@Test
	public void testAddComment() throws Exception {
		String hotelName = "Salgados"; 
		int hotelid = 100; 
		mvc.perform(post("/hotels")
				.param("id", Integer.toString(hotelid))
                .param("name", hotelName)
		        .param("address", "asaS")
		        .param("ownerid", "1")
		        .param("category", "hotel")
		        .param("rating", "3").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

	    mvc.perform(get("/hotels/{id}.json", Integer.toString(hotelid)).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
				
		Hotel hotel = hotels.findByName(hotelName);
		
		Assert.assertTrue(hotel != null);
	}
	
	@Test
	public void testEditHotel() throws Exception {
		String hotelName = "Estado"; 
		int hotelid = 100; 
		mvc.perform(post("/hotels")
				.param("id", Integer.toString(hotelid))
                .param("name", hotelName)
		        .param("address", "asaS")
		        .param("ownerid", "1")
		        .param("category", "hotel")
		        .param("rating", "3").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"));

	    mvc.perform(get("/hotels/{id}.json", Integer.toString(hotelid)).accept(MediaType.APPLICATION_JSON))
	    .andExpect(status().isOk())
		.andExpect(content().contentType("application/json;charset=UTF-8"));
	    //.andExpect(jsonPath("name").value(hotelName));
				
		Hotel hotel = hotels.findByName(hotelName);
		
		Assert.assertTrue(hotel != null);
	}
	
	@Test
	public void testDeleteHotel() throws Exception {
		String hotelName = "Estado"; 
		int hotelid = 100; 
		mvc.perform(delete("/hotels/{id}", Integer.toString(hotelid))
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

	    mvc.perform(get("/hotels/{id}.json", Integer.toString(hotelid)).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
				
		Hotel hotel = hotels.findByName(hotelName);
		
		Assert.assertTrue(hotel == null);
	}
	
//	@Test
//	public void testGetOne() throws Exception {
//		String hotelName = "Marriot"; 
//
//		Hotel hotel = hotels.findByName(hotelName);
//		
//		mvc.perform(get("/hotels/"+hotel.getId()))
//				.andExpect(view().name("hotels/show"));
//	}
	
//	@Test
//	public void testModel() throws Exception {
//		mvc.perform(get("/hotels"))
//				.andExpect(model().attributeExists("hotels"));
//	}
}

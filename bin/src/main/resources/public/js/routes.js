'use strict';

ciaiApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.    
    when("/",{
    	templateUrl: 'views/home.html',
    	controller: 'homeController'
    }).
    when("/hotels/new", {
        templateUrl: 'views/hotels/edit.html',
        controller: 'hotelsEditController'
    }).
    otherwise({
        redirectTo: '/',
    });
}]);